#!/bin/bash
ssh admin@185.189.13.148 'mkdir -p /home/admin/www/container'
scp -r ./efDev admin@185.189.13.148:/home/admin/www/container
scp -r ./front-end/build admin@185.189.13.148:/home/admin/container/build
# scp docker-compose.yml admin@185.189.13.148:/home/admin/www/container
npm i --production
# ssh admin@185.189.13.148 'cd ~/www/container/efDev && npm i --production'
# ssh admin@185.189.13.148 'cd ~/www/container && docker-compose down'
ssh admin@185.189.13.148 'cd ~/www/container && pm2 start bin/www --name app1 -i 2'